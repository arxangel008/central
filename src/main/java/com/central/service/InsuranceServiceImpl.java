package com.central.service;

import com.central.dao.InsuranceDao;
import com.central.model.Insurance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Slf4j
@Service
public class InsuranceServiceImpl implements InsuranceService {

    private final InsuranceDao insuranceDao;

    @Autowired
    public InsuranceServiceImpl(InsuranceDao insuranceDao) {
        this.insuranceDao = insuranceDao;
    }

    @Transactional
    @Override
    public void save(Insurance insurance) {
        if (insurance == null) {
            throw new IllegalArgumentException("Нет информации о новой организации");
        }

        try {
            insuranceDao.insert(insurance);
        } catch (Exception e) {
            log.error("Ошибка при сохранении организации");
            throw new IllegalArgumentException("Ошибка при сохранении организации");
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<Insurance> getByFilter(Insurance insurance) {
        try {
            return insuranceDao.getByFilter(insurance);
        } catch (Exception e) {
            log.error("Ошибка при получении организаций");
            throw new IllegalArgumentException("Ошибка при получении организаций");
        }
    }
}
