package com.central.controller;

import com.central.converter.ConvertDtoToModel;
import com.central.dto.InsuranceRequestDto;
import com.central.model.Insurance;
import com.central.service.InsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collection;

@Controller
public class InsuranceController {

    private final InsuranceService insuranceService;
    private final ConvertDtoToModel convertDtoToModel;

    @Autowired
    public InsuranceController(InsuranceService insuranceService, ConvertDtoToModel convertDtoToModel) {
        this.insuranceService = insuranceService;
        this.convertDtoToModel = convertDtoToModel;
    }

    @GetMapping("/")
    public String index(Model model) {

        Collection<Insurance> insurances = insuranceService.getByFilter(null);

        model.addAttribute("insurances", insurances);

        return "index";
    }

    @GetMapping("/filter")
    public String getByFilter(Model model, @ModelAttribute("insuranceRequestDto") InsuranceRequestDto insuranceRequestDto) {

        Collection<Insurance> insurances = insuranceService.getByFilter(convertDtoToModel.convert(insuranceRequestDto));

        model.addAttribute("insurances", insurances);

        return "index";
    }

    @PostMapping("/add")
    public String addNew(Model model, @ModelAttribute("insuranceRequestDto") InsuranceRequestDto insuranceRequestDto) {
        try {
            insuranceService.save(convertDtoToModel.convert(insuranceRequestDto));
        } finally {
            return index(model);
        }
    }
}
