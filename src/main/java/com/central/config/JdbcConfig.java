package com.central.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class JdbcConfig {

    @Value("${spring.datasource.driverClassName}")
    private String driver;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.poolSizeMax}")
    private int poolSizeMax;

    private HikariDataSource dataSource;

    @Primary
    @Bean(name = "dataSource")
    public DataSource configureDataSource() {
        final HikariConfig config = new HikariConfig();
        config.setDriverClassName(driver);
        config.setRegisterMbeans(false);
        config.setPoolName("courierHikariCP");
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        config.setConnectionTimeout(0);
        config.setMaximumPoolSize(poolSizeMax);
        config.setAutoCommit(false);
        dataSource = new HikariDataSource(config);
        return dataSource;
    }
}