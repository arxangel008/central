package com.central.config;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class FlyWayConfig {

    @Value("${flyway.locations}")
    private String locations;

    private final DataSource dataSource;

    @Autowired
    public FlyWayConfig(@Qualifier("dataSource") DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean(initMethod = "migrate", name = "flyway")
    public Flyway flyway() {
        final Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(false);
        flyway.setLocations(locations);
        flyway.setDataSource(dataSource);
        return flyway;
    }
}
