package com.central.dto;

import lombok.*;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ToString
public class InsuranceRequestDto {

    /**
     * Наименование
     */
    String name;

    /**
     * Регион/Область
     */
    String region;

    /**
     * Город
     */
    String city;

    /**
     * Адрес
     */
    String address;

    /**
     * ИНН
     */
    Long inn;

    /**
     * ОГРН
     */
    Long ogrn;

    /**
     * Дом
     */
    Integer house;
}
