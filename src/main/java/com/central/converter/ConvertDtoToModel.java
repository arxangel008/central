package com.central.converter;

import com.central.dto.InsuranceRequestDto;
import com.central.model.Insurance;
import org.springframework.stereotype.Component;

@Component
public class ConvertDtoToModel {

    public Insurance convert(InsuranceRequestDto dto) {
        return Insurance.builder()
                .name(dto.getName())
                .region(dto.getRegion())
                .city(dto.getCity())
                .house(dto.getHouse())
                .address(dto.getAddress())
                .inn(dto.getInn())
                .ogrn(dto.getOgrn())
                .build();
    }
}
