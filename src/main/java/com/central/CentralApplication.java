package com.central;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = "com.central.*")
@SpringBootApplication
public class CentralApplication {
    public static void main(String[] args) {
        SpringApplication.run(CentralApplication.class);
    }
}
