package com.central.model;

import lombok.*;

/**
 * Модель страховых организаций
 */
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ToString
public class Insurance {

    /**
     * Идентификатор
     */
    Long id;

    /**
     * Наименование
     */
    String name;

    /**
     * Регион/Область
     */
    String region;

    /**
     * Город
     */
    String city;

    /**
     * Адрес
     */
    String address;

    /**
     * Дом
     */
    Integer house;

    /**
     * ИНН
     */
    Long inn;

    /**
     * ОГРН
     */
    Long ogrn;
}
