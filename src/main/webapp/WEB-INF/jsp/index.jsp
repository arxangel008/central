<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=utf-8" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Person List</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<h1>Страховые организации</h1>

<br/><br/>
<div>
    <h3>Фильтры поиска</h3>
    <form method="get" action="filter">
        Регион <input type="text" name="region">
        Город <input type="text" name="city">
        Адрес <input type="text" name="address">
        <br>
        Наименование <input type="text" name="name">
        ИНН <input type="number" name="inn" max="9999999999" size="150">
        ОГРН <input type="number" name="ogrn" max="9999999999999" size="150">
        <br>
        <input type="submit" value="Поиск" maxlength="50">
    </form>

</div>
<br>
<div id="insurances-table">
    <table border="1">
        <tr>
            <th>Наименование</th>
            <th>Регион</th>
            <th>Город</th>
            <th>Адрес</th>
            <th>Дом</th>
            <th>ИНН</th>
            <th>ОГРН</th>
        </tr>
        <c:forEach items="${insurances}" var="insurance">
            <tr>
                <td>${insurance.name}</td>
                <td>${insurance.region}</td>
                <td>${insurance.city}</td>
                <td>${insurance.address}</td>
                <td>${insurance.house}</td>
                <td>${insurance.inn}</td>
                <td>${insurance.ogrn}</td>
            </tr>
        </c:forEach>
    </table>
</div>

<div>
    <h3>Внос новой организации:</h3>
    <form method="post" action="add">
        Регион <input type="text" name="region">
        Город <input type="text" name="city">
        Адрес <input type="text" name="address">
        Дом <input type="number" name="house">
        <br>
        Наименование <input type="text" name="name">
        ИНН <input type="number" name="inn">
        ОГРН <input type="number" name="ogrn">
        <br>
        <input type="submit" value="Запись" maxlength="50">
    </form>
</div>
</body>

</html>