package com.central;

import com.central.dao.InsuranceDao;
import com.central.model.Insurance;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@Sql("/test-values/InsuranceTestValues.sql")
public class InsuranceDaoTest {

    @Autowired
    private InsuranceDao insuranceDao;

    @Test
    public void insert(){

        Insurance insurance = Insurance.builder()
                .name("Тестовая организация")
                .address("ул. Тестовая")
                .city("г. Тестовый")
                .house(1)
                .inn(1111111111L)
                .ogrn(1111111111111L)
                .region("Тестовая обл.")
                .build();
        insuranceDao.insert(insurance);
        insurance = insuranceDao.getById(insurance.getId());
        Assert.assertNotNull(insurance);
        Assert.assertNotNull(insurance.getId());
    }

    @Test
    public void getByName() {
        Insurance insurance = Insurance.builder()
                .name("Нов")
                .build();
        Collection<Insurance> insurances = insuranceDao.getByFilter(insurance);
        Assert.assertNotNull(insurances);
        Assert.assertEquals(2, insurances.size());
    }

    @Test
    public void getByRegion() {
        Insurance insurance = Insurance.builder()
                .region("Нов")
                .build();
        Collection<Insurance> insurances = insuranceDao.getByFilter(insurance);
        Assert.assertNotNull(insurances);
        Assert.assertEquals(2, insurances.size());
    }

    @Test
    public void getByCity() {
        Insurance insurance = Insurance.builder()
                .city("Кеме")
                .build();
        Collection<Insurance> insurances = insuranceDao.getByFilter(insurance);
        Assert.assertNotNull(insurances);
        Assert.assertEquals(1, insurances.size());
    }

    @Test
    public void getByAddress() {
        Insurance insurance = Insurance.builder()
                .address("Кр")
                .build();
        Collection<Insurance> insurances = insuranceDao.getByFilter(insurance);
        Assert.assertNotNull(insurances);
        Assert.assertEquals(1, insurances.size());
    }

    @Test
    public void getByInn() {
        Insurance insurance = Insurance.builder()
                .inn(1111111112L)
                .build();
        Collection<Insurance> insurances = insuranceDao.getByFilter(insurance);
        Assert.assertNotNull(insurances);
        Assert.assertEquals(1, insurances.size());
    }

    @Test
    public void getByOgrn() {
        Insurance insurance = Insurance.builder()
                .ogrn(1111111111113L)
                .build();
        Collection<Insurance> insurances = insuranceDao.getByFilter(insurance);
        Assert.assertNotNull(insurances);
        Assert.assertEquals(1, insurances.size());
    }

    @Test
    public void getByWithoutParam() {
        Collection<Insurance> insurances = insuranceDao.getByFilter(null);
        Assert.assertNotNull(insurances);
        Assert.assertEquals(3, insurances.size());
    }
}
